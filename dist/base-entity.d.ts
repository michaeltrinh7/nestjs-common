import { Key } from './key.type';
export declare class BaseEntity {
    id: Key;
    createdAt: Date;
    modifiedAt: Date;
}
