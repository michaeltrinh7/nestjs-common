import { Repository } from 'typeorm';
import { BaseEntity } from './base-entity';
import { Key } from './key.type';
export interface IRepository<E extends BaseEntity> {
    getRepository(): Repository<E>;
    getAll(): Promise<E[]>;
    get(id: Key): Promise<E>;
    update(entity: E): Promise<E>;
    create(entity: E): Promise<Key>;
    delete(id: Key): void;
}
